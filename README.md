 erlcfgparse
 ~~~~~~~~~~~~~

 Event-based sequential access parser for consuming Erlang config files in Python

 :copyright: 2013 by Andy Bailey <gooseyard@gmail.com>, see AUTHORS for more details
 :license: license_name, see LICENSE for more details
