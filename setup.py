from setuptools import setup

config = {
    'description': 'Erlang Config Parser',
    'author': 'Andy Bailey',
    'author_email': '<gooseyard@gmail.com>',
    'url': '',
    'download_url': '',
    'version': '0',
    'install_requires': ['nose'],
    'packages': ['erlcfgparse'],
    'name': 'erlcfgparse',
}

setup(**config)
