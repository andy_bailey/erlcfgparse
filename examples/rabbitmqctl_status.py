#!/usr/bin/env python

import sys
import urllib2
import erlcfgparse
from rabbitmqctl_status_handler import RabbitmqctlStatusHandler
import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("file",
                        help="read from FILE",
                        type=argparse.FileType('r'),
                        nargs='?',
                        default=sys.stdin)

    args = parser.parse_args()
    erlp = erlcfgparse.ErlconfigParser()

    cfg = {}
    handler = RabbitmqctlStatusHandler(cfg)
    erlp.parse(args.file, handler)
    print cfg

if __name__ == "__main__":
    main()
