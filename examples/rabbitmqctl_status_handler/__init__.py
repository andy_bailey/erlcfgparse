from erlcfgparse import ErlconfigHandler

class RabbitmqctlStatusHandler(ErlconfigHandler):
    def __init__(self, data={}):
        self.data = data
        self.ctx = []
        self.curelem = None

    def start_list(self, value):
        self.ctx.append([])
        self.curelem = self.ctx[-1]

    def start_tuple(self, value):
        self.ctx.append([])
        self.curelem = self.ctx[-1]

    def handle_separator(self, value):
        pass

    def end_list(self, value):
        pass

    def end_tuple(self, value):
        if len(self.curelem) == 0:
            return

        print("at end of tuple, curelem is %s" % self.curelem)
        self.data[self.curelem.pop(0)] = self.curelem

    def handle_identifier(self, value):
        self.curelem.append(value)

    def handle_quoted_string(self, value):
        self.curelem.append(value[1:-1])

    def handle_binary(self, value):
        self.curelem.append(value[3:-3])

    def handle_newline(self, value):
        pass
