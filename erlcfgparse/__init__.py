import re
import logging

# coding: utf-8
"""
    erlcfgparse
    ~~~~~~~~~~~~~

    Event-based sequential access parser for consuming Erlang config files in Python

    :copyright: 2013 by Andy Bailey <gooseyard@gmail.com>, see AUTHORS for more details
    :license: license_name, see LICENSE for more details
"""

logger = logging.getLogger('erlcfgparse')
logger.setLevel(logging.DEBUG)
fmtr = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

import sys
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
ch.setFormatter(fmtr)
logger.addHandler(ch)

class ErlconfigHandler():
    def __init__(self):
        pass

    def start_list(self, val):
        pass

    def end_list(self, val):
        pass

    def start_tuple(self, val):
        pass

    def handle_separator(self, val):
        pass

    def end_tuple(self, val):
        pass

    def handle_identifier(self, val):
        pass

    def handle_element(self, val):
        pass

    def handle_float(self, val):
        pass

    def handle_any(self, val):
        pass

    def handle_quoted_string(self, val):
        pass

    def handle_binary(self, val):
        pass

class ErlconfigParser():

    def __init__(self):
        self.ctx = None
        self.cur_elem = None

    def parse(self, stream, handler):
        scanner=re.Scanner([
            (r"%+.*",  lambda scanner,token:("COMMENT", token)),
            (r"<<.*>>", lambda scanner,token:("BINARY", token)),
            (r'"[^"]+"', lambda scanner,token:("QUOTED_STRING", token)),
            (r"[0-9]+\.[0-9]+", lambda scanner,token:("FLOAT", token)),
            (r"[a-zA-Z0-9:_]+", lambda scanner,token:("IDENTIFIER", token)),
            (r"[\[]", lambda scanner,token:("START_LIST", token)),
            (r"[\]]", lambda scanner,token:("END_LIST", token)),
            (r"[\{]", lambda scanner,token:("START_TUPLE", token)),
            (r"[\}]", lambda scanner,token:("END_TUPLE", token)),
            (r"[\,]+", lambda scanner,token:("SEPARATOR", token)),
            (r"[\"]+", lambda scanner,token:("QUOTE", token)),
            (r"\s+", None), # None == skip token.
        ])

        dispatch = { 'IDENTIFIER' : 'handle_identifier',
                     'FLOAT' : 'handle_float',
                     'QUOTED_STRING' : 'handle_quoted_string',
                     'BINARY' : 'handle_binary',
                     'START_LIST' : 'start_list',
                     'END_LIST' : 'end_list',
                     'START_TUPLE' : 'start_tuple',
                     'END_TUPLE' : 'end_tuple',
                     'SEPARATOR' : 'handle_separator',
                     'COMMENT': 'handle_any',
                     'NEWLINE': 'handle_newline',
                      }

        results, remainder=scanner.scan(stream.read())
        if remainder != ".\n":
            logger.warn("scanner failure, remainder: %s" % remainder)

        stream = iter(results)

        done = False
        while not done:
            try:
                (tp, val) = stream.next()
                if tp == None:
                    continue

                if tp == "COMMENT":
                    continue

                meth = getattr(handler.__class__, dispatch[tp])
                meth(handler, val)
            except StopIteration:
                done = True
